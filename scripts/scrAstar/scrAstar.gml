/// scrAstar(startX, startY, finishX, finishY)
var gridSize = 32;

var startX = argument[0] div gridSize;
var startY = argument[1] div gridSize;
var finishX = argument[2] div gridSize;
var finishY = argument[3] div gridSize;

var closedSet = ds_list_create();
var openSet = ds_list_create();


//Number of cells
var ww = (room_width/gridSize);
var hh = (room_height/gridSize);
var strDebug = "";

var cameFrom = ds_grid_create(ww, hh);
ds_grid_clear(cameFrom, -1);

var initialPosition = (startY * ww) + startX; // this should give the number of the position

//Math behind it:
/*

	Suppose this is the grid
	  0  1  2  3  4  5  6  7  8  9
	0 0  1  2  3 ...
	1 10 11 12 13
	2 
	3 
	4 
	5
	6
	7
	8
	9 90 91

	
	so the cell in (0,0) would be 0*10 + 0, = 0
	and the cell in (1,0) would be (10*0) + 0 = 10
	
	This way, I don't need to store the X and Y of the grid, just the number of the cell
	so I don't need to create lots of grids, just lots of lists
	Hope future me don't get confused with this shit
	
*/

ds_list_add(openSet, initialPosition);

/// Create the GRID for the map
var gameMap = ds_grid_create(ww, hh);
for(var yy =0; yy < hh; yy++) {
	strDebug += "\n";
	for (var xx = 0; xx < ww; xx++) {
		var posX = xx * gridSize;
		var posY = yy * gridSize;
		if(position_meeting(posX, posY, objCollision)) {
			gameMap[# xx, yy] = -1;
			//Not sure if it should be like this, It's 1am and I'm half asleep:
			var cellPosition = (yy * ww) + xx;
			ds_list_add(closedSet, cellPosition);
			strDebug += "# ";
		} else {
			gameMap[# xx, yy] = 1;
			strDebug += "1 ";
		}
	}
}
show_debug_message(strDebug);

//Create the gScore grid, and set the initial position to 0, since there's no cost to go to the
//sameplace you are
var gScore = ds_grid_create(ww, hh);
ds_grid_clear(gScore, 999999);
gScore [# startX, startY] = 0;

//Create the fScore grid, and set it all to "9999999999"(considered infinite in this case)
var fScore = ds_grid_create(ww, hh);
ds_grid_clear(fScore, 9999999999);
fScore [# startX, startY] = abs(startX - finishX) + abs(startY - finishY);

//Create the path to go
var pathToGo = path_add();

while(!ds_list_empty(openSet)) {
	var lowestScore = 999999999;
	var yyLower = -1;
	var xxLower = -1;
	var yyPositionCurrent = -1;
	var xxPositionCurrent = -1;
	var currentOpenSet = -1;
	for(var i = 0; i<ds_list_size(openSet); i++){
		yyPositionCurrent = openSet[| i] div ww;
		xxPositionCurrent = openSet[| i] mod ww;
		if(fScore[# xxPositionCurrent, yyPositionCurrent] < lowestScore) {
			lowestScore = fScore[# xxPositionCurrent, yyPositionCurrent];
			yyLower = yyPositionCurrent;
			xxLower = xxPositionCurrent;
			currentOpenSet = i;
		}
	}
	
	if(finishX == xxLower and finishY == yyLower) {
		path_add_point(pathToGo, finishX * gridSize, finishY * gridSize, 100);
		while(ds_grid_get(cameFrom, xxLower, yyLower) != -1) {
			var cellToGo = ds_grid_get(cameFrom, xxLower, yyLower);
			xxLower = cellToGo mod ww;
			yyLower = cellToGo div ww;
			path_add_point(pathToGo, xxLower * gridSize, yyLower * gridSize, 100);
		}
		path_reverse(pathToGo);
		return pathToGo;
	}
	
	var addToClosed = ds_list_find_value(openSet, currentOpenSet);
	ds_list_delete(openSet, currentOpenSet);
	ds_list_add(closedSet, addToClosed);
	
	//Won't go in diagonal for now
	var yyNeighbour = -1;
	var xxNeighbour = -1;
	for(var pos =0; pos< 4; pos++) {
		//Check the 4 positions
		switch(pos){
			//Up
			case 0:
				if(yyLower > 0) {
					yyNeighbour = yyLower - 1;
					xxNeighbour = xxLower;
				} else {
					continue;
				}
				break;
			//Down
			case 1:
				if(yyLower < hh-1) {
					yyNeighbour = yyLower + 1;
					xxNeighbour = xxLower;
				} else{
					continue;
				}
				break;
			//Left
			case 2:
				if(xxLower > 0) {
					xxNeighbour = xxLower - 1;
					yyNeighbour = yyLower;
				} else {
					continue;
				}
				break;
			//Right
			case 3:
				if(xxLower < ww-1) {
					xxNeighbour = xxLower + 1;
					yyNeighbour = yyLower;
				} else {
					continue;
				}
				break;
		}	
		var setConvertedNeighbour = (yyNeighbour * hh) + xxNeighbour; //This will give the number of the cell in the grid
		//If it's already on the closed set, break it
		if(ds_list_find_index(closedSet, setConvertedNeighbour) != -1) {
			continue;
		}
		
		var tentativeGScore = gScore[# xxLower, yyLower] + abs(xxLower - xxNeighbour) + abs(yyLower - yyNeighbour);
		if(ds_list_find_index(openSet, setConvertedNeighbour) == -1) {
			ds_list_add(openSet, setConvertedNeighbour);
		} else if(tentativeGScore >= gScore[# xxNeighbour, yyNeighbour]) {
			continue; // This path is equal or worse
		}
		
		//If this path is the best, save it
		cameFrom[# xxNeighbour, yyNeighbour] = (yyLower * hh) + xxLower;
		gScore[# xxNeighbour, yyNeighbour] = tentativeGScore;
		fScore[# xxNeighbour, yyNeighbour] = tentativeGScore + abs(xxNeighbour - finishX) + abs(yyNeighbour - finishY);
		var debugCameFrom = cameFrom[# xxNeighbour, yyNeighbour];
		var debugGScore = gScore[# xxNeighbour, yyNeighbour];
		var debugFScore = fScore[# xxNeighbour, yyNeighbour];
	}
}
//If it gets here, it failed to get a path
return -1;
	