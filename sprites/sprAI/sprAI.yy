{
    "id": "6b4b62aa-5b8e-433f-86f3-c92ad1f74b9e",
    "modelName": "GMSprite",
    "mvc": "1.11",
    "name": "sprAI",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a1f740c7-32d2-4620-8c46-c953a6cfdf1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b4b62aa-5b8e-433f-86f3-c92ad1f74b9e",
            "compositeImage": {
                "id": "b63eed6b-9a54-4fc7-a984-0f7535293953",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1f740c7-32d2-4620-8c46-c953a6cfdf1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab303b83-1c91-4694-b620-950212023642",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1f740c7-32d2-4620-8c46-c953a6cfdf1a",
                    "LayerId": "f6e3495c-48c1-4579-8052-61d1fa5d2c36"
                }
            ]
        }
    ],
    "height": 32,
    "layers": [
        {
            "id": "f6e3495c-48c1-4579-8052-61d1fa5d2c36",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b4b62aa-5b8e-433f-86f3-c92ad1f74b9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}