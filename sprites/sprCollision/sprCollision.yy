{
    "id": "608db46a-43d2-41cd-88a7-4b2901d149e9",
    "modelName": "GMSprite",
    "mvc": "1.11",
    "name": "sprCollision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "74a73cf1-783d-474e-8c30-81612c9e29d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "608db46a-43d2-41cd-88a7-4b2901d149e9",
            "compositeImage": {
                "id": "7e11e56b-64eb-4148-b555-505743e72c0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74a73cf1-783d-474e-8c30-81612c9e29d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4880d852-c611-4e66-b593-2556ff35892a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74a73cf1-783d-474e-8c30-81612c9e29d2",
                    "LayerId": "ebdbc982-f659-4c00-9091-3c007947da06"
                }
            ]
        }
    ],
    "height": 32,
    "layers": [
        {
            "id": "ebdbc982-f659-4c00-9091-3c007947da06",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "608db46a-43d2-41cd-88a7-4b2901d149e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "textureGroup": 0,
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}