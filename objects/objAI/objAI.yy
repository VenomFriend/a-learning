{
    "id": "9c38e086-27e1-4a21-b6b0-4550915bdf9e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objAI",
    "eventList": [
        {
            "id": "b59bf789-034d-4f87-a4c7-ef425b0beb46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "9c38e086-27e1-4a21-b6b0-4550915bdf9e"
        },
        {
            "id": "f0091873-94eb-4737-a7de-9caa1ecdbf1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9c38e086-27e1-4a21-b6b0-4550915bdf9e"
        },
        {
            "id": "f8dc8303-0435-4feb-ab2b-6b300e73aa34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9c38e086-27e1-4a21-b6b0-4550915bdf9e"
        },
        {
            "id": "159afa32-60da-4721-9931-a70740eb397a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9c38e086-27e1-4a21-b6b0-4550915bdf9e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "6b4b62aa-5b8e-433f-86f3-c92ad1f74b9e",
    "visible": true
}