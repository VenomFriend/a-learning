/// @description Insert description here
// You can write your code in this editor

pathClick = scrAstar(x, y, mouse_x, mouse_y);
show_debug_message("Clicked! Result: ");
show_debug_message(pathClick);
path_start(pathClick, 3, path_action_stop, true);
var debugString = "";
for(var i =0; i < path_get_number(pathClick); i++) {
	debugString += "\nx: " + string(path_get_point_x(pathClick, i)) + "\ny: " + string(path_get_point_y(pathClick, i));
}
endX = path_get_point_x(pathClick, path_get_number(pathClick) - 1);
endY = path_get_point_y(pathClick, path_get_number(pathClick) - 1);
show_debug_message(debugString);